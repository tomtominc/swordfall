﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallMovement : Movement
{
    [SerializeField]
    protected float m_fallSpeed;

    public override void gainingControl()
    {
       
    }

    public override void losingControl()
    {
      
    }

    public override bool wantsControl()
    {
        return true;
    }

    public override Vector2 move()
    {
        return new Vector2(0f, -m_fallSpeed);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Movement : MonoBehaviour
{
    protected GameController m_gameController;
    protected Entity m_entity;

    protected Transform m_transform
    {
        get { return m_entity.transform; }
    }

    public virtual void initialize(GameController p_gameController, Entity p_entity)
    {
        m_gameController = p_gameController;
        m_entity = p_entity;
    }

    public abstract bool wantsControl();

    public abstract void gainingControl();

    public abstract void losingControl();

    public abstract Vector2 move();

}

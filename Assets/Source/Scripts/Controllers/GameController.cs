﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public void Awake()
    {
        Entity[] l_entities = FindObjectsOfType < Entity >();

        for (int i = 0; i < l_entities.Length; i++)
        {
            l_entities[i].initialize(this);
        }
    }
}

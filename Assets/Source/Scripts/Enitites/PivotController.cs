﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlatformerPro;

public class PivotController : MonoBehaviour
{
    [SerializeField]
    private Transform m_attachment;

    [SerializeField]
    private List < Pivot > m_pivots;

    private Character m_character;

    private void Start()
    {
        m_character = GetComponentInParent < Character >();
        m_character.ChangeAnimationState += onChangedAnimationState;
    }

    private void onChangedAnimationState(object p_sender, AnimationEventArgs p_args)
    {
        Pivot l_pivot = m_pivots.Find(x => x.animationState == p_args.State);

        if (l_pivot == null)
            Debug.LogError(p_args.State);
        
        l_pivot.attach(m_attachment);
    }

}

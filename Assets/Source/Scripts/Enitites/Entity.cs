﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Entity : MonoBehaviour
{
    protected GameController m_gameController;
    protected SpriteRenderer m_renderer;
    protected SpriteAnimation m_animator;
    protected List < Movement > m_movements;

    protected Movement m_currentMovement;
    protected Vector2 m_velocity;

    public virtual void initialize(GameController p_gameController)
    {
        m_gameController = p_gameController;
        m_movements = GetComponentsInChildren < Movement >().ToList();

        configureMovements();
    }

    public virtual void configureMovements()
    {
        foreach (Movement l_movement in m_movements)
        {
            l_movement.initialize(m_gameController, this);
        }
    }

    public virtual void onEnable()
    {
        
    }

    public virtual void update()
    {
        checkMovements();
        updateMovement();
    }

    public virtual void onDestroy()
    {
        
    }

    public virtual void destroy()
    {
        
    }

    protected virtual void checkMovements()
    {
        Movement l_lastMovement = m_currentMovement;

        foreach (Movement l_movement in m_movements)
        {
            if (l_movement.wantsControl())
            {
                m_currentMovement = l_movement;
                break;
            }
        }

        if (l_lastMovement != m_currentMovement)
        {
            if (l_lastMovement != null)
                l_lastMovement.losingControl();
            
            m_currentMovement.gainingControl();
        }

    }

    protected virtual void updateMovement()
    {
        Vector2 l_velocity = m_currentMovement.move();
        m_velocity += l_velocity * Time.deltaTime;
        transform.position += (Vector3)m_velocity;
    }

    #region Unity Hooks

    private void Update()
    {
        update();
    }

    private void OnDisable()
    {
        onDestroy();
    }

    private void OnEnable()
    {
        onEnable();
    }

    #endregion
}

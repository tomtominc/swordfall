﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Pivot : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer m_characterSprite;
    [SerializeField]
    private PlatformerPro.AnimationState m_animationState;
    [SerializeField]
    private float m_angle;
  
    private Vector2 m_pivot;

    public PlatformerPro.AnimationState animationState
    {
        get { return m_animationState; }
    }


    public void Start()
    {
        m_pivot = transform.localPosition;
    }

    public void attach(Transform p_attachment)
    {
        p_attachment.eulerAngles = new Vector3(0f, 0f, m_angle);
        p_attachment.GetComponent < SpriteRenderer >().flipY = m_characterSprite.transform.localScale.x == -1;
        p_attachment.SetParent(transform);
        p_attachment.localPosition = Vector3.zero;

        Vector2 l_pivot = new Vector2(m_pivot.x * m_characterSprite.transform.localScale.x, m_pivot.y);
        
        transform.localPosition = l_pivot;
    }
        
}
